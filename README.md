## Sonix dotfiles

Slonix supports configuring vim, zsh, tmux, screen, XResources and awesome. Just run:

```
./setup.sh
```

and enjoy.
