#!/bin/bash

#
# COLORS
#
C_S="\033[0;0m"  # RESET
C_Y="\033[1;33m" # YELLOW
C_R="\033[1;31m" # RED
C_G="\033[1;32m" # GREEN
C_B="\033[1;34m" # BLUE

droot="$PWD"
dhome="$HOME"
dold="${dhome}/.old/"

function install_ohmyzsh() {
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}

function install_vimplug() {
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
}

function link_dotfiles() {
  for currentfile in ${droot}/dotfiles/*
  do
    basefile=$(basename $currentfile)
    homefile="${dhome}/.${basefile}"
    backupfile="${dold}/${basefile}"

    if [ -e "${homefile}" -o -h "${homefile}" ]
    then
      if [ -d "${homefile}" ]
      then
        echo -e "Saving backup of directory '.${basefile}' ..."
      else
        echo -e "Saving backup of file '.${basefile}' ..."
      fi
      mv "${homefile}" "${backupfile}" \
        && echo -e "$C_S[$C_G  OK  $C_S]" \
        || echo -e "$C_S[$C_R FAIL $C_S]"
    fi


    if [ -d "${homefile}" ]
    then
      echo -e "Linking directory '.${basefile}' ..."
    else
      echo -e "Linking file '.${basefile}' ..."
    fi

    ln -s "${currentfile}" "${homefile}" \
      && echo -e "$C_S[$C_G  OK  $C_S]" \
      || echo -e "$C_S[$C_R FAIL $C_S]"
  done

  if [ -f ${droot}/dotfiles/rc.lua ]; then
    echo "Linking awesome config..."
    ln -s ${droot}/dotfiles/rc.lua ~/.config/awesome/rc.lua
    ln -s ${droot}/dotfiles/xrandr.lua ~/.config/awesome/xrandr.lua
  fi
}

function install_awesome_config() {
  if [ ! -d ~/.config/awesome ];
  then
    mkdir -p ~/.config/awesome
  fi
  rm -rf /tmp/awesome-copycats
  git clone --recursive https://github.com/lcpz/awesome-copycats.git /tmp/awesome-copycats
  mv -bv /tmp/awesome-copycats/* ~/.config/awesome; rm -rf /tmp/awesome-copycats
}

function install_xresources_themes() {
  if [ ! -d ~/.xresources-themes ];
  then
    mkdir -p ~/.xresources-themes
  fi
  rm -rf /tmp/Xresources-themes
  git clone --recursive https://github.com/logico-dev/Xresources-themes /tmp/Xresources-themes
  mv -bv /tmp/Xresources-themes/* ~/.xresources-themes/; rm -rf /tmp/Xresources-themes
}

function install_hack_font() {
  rm -rf /tmp/ttf
  wget https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip -O /tmp/hack.zip
  (cd /tmp && unzip hack.zip)
  mkdir ~/.local/share/fonts
  mv -bv /tmp/ttf/* ~/.local/share/fonts/; rm -rf /tmp/ttf /tmp/hack.zip
  fc-cache -f -v
}

function install_tpm() {
  rm -rf ~/.tmux/plugins/tpm
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
}

function main() {
  PS3='Please enter your choice: '
  options=("zsh" "dotfiles" "vimplug" "awesome" "xresources" "hack" "exit")
  select opt in "${options[@]}"
  do
      case $opt in
          "zsh")
             echo "Installing oh-my-zeh..."
             install_ohmyzsh
             exit
        ;;
          "dotfiles")
            echo "Linking dotfiles..."
            link_dotfiles
            exit
        ;;
          "vimplug")
            echo "Installing vim-plug..."
            install_vimplug
            exit
        ;;
          "awesome")
            echo "Installing awecome config..."
            install_awesome_config
            exit
        ;;
          "xresources")
            echo "Installing XResources themes..."
            install_xresources_themes
            exit
        ;;
          "hack")
            echo "Installing hack font..."
            install_hack_font
            exit
        ;;
          "tmux plugin manager")
            echo "Installing tmux plugin manager..."
            install_tpm
            exit
        ;;
          "all")
            echo "Stand back..."
            install_ohmyzsh
            install_vimplug
            install_awesome_config
            install_xresources_themes
            install_hack_font
            install_tpm
            link_dotfiles
            exit
        ;;
          "Quit")
             exit
        ;;
          *) echo invalid option;;
      esac
  done
}

main
